package org.spigot.reticle.API;

import java.lang.Thread.State;
import java.lang.reflect.Method;

import org.spigot.reticle.storage;

/**
 * Safe thread. Interrupted when plugin is unloaded
 * @author Encorn
 *
 */
public abstract class ReticleThread {
	private pThread thread;

	public ReticleThread(Plugin Plugin) {
		this.thread = new pThread(this, Plugin);
	}

	public final void start() {
		thread.start();
	}

	@SuppressWarnings("deprecation")
	public final void stop() {
		thread.stop();
	}

	public final void interrupt() {
		thread.interrupt();
	}

	public final void join() throws InterruptedException {
		thread.join();
	}

	public final State getState() {
		return thread.getState();
	}

	public final boolean isAlive() {
		return thread.isAlive();
	}

	public abstract void run();

	private class pThread extends Thread {
		private ReticleThread main;
		private Plugin Plugin;

		private pThread(ReticleThread main, Plugin pl) {
			this.main = main;
			this.Plugin = pl;
		}

		@Override
		public void run() {
			try {
				Method add = storage.pluginManager.getClass().getDeclaredMethod("addThread", Plugin.class, ReticleThread.class);
				Method remove = storage.pluginManager.getClass().getDeclaredMethod("removeThread", Plugin.class, ReticleThread.class);
				add.setAccessible(true);
				add.invoke(storage.pluginManager, Plugin, main);
				add.setAccessible(false);
				main.run();
				remove.setAccessible(true);
				remove.invoke(storage.pluginManager, Plugin, main);
				remove.setAccessible(false);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
