package org.spigot.reticle.API;

import org.spigot.reticle.storage;

public final class Logger {

	private final String name;

	protected Logger(Plugin plugin) {
		this.name = storage.pluginManager.getPluginInfo(plugin).Name;
	}

	public void Log(String Message) {
		storage.conlog("[" + name + "] " + Message);
	}
	

}
