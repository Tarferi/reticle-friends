package org.spigot.reticle.API.Menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

import javax.swing.JMenuItem;

import org.spigot.reticle.PluginInfo;
import org.spigot.reticle.storage;
import org.spigot.reticle.API.Plugin;
import org.spigot.reticle.botfactory.mcbot;

public class AbstractMenuItem {
	protected final JMenuItem item;

	/**
	 * Returns menu object itself. Not safe to use
	 * 
	 * @return Returns menu object
	 */
	public JMenuItem getItemObject() {
		return item;
	}

	/**
	 * Returns item name
	 * 
	 * @return Returns item name
	 */
	public String getItemName() {
		return item.getText();
	}

	protected AbstractMenuItem(JMenuItem item) {
		this.item = item;
	}

	/**
	 * Adds listener to current menu item.
	 * 
	 * @param Plugin
	 *            Instance of invoking plugin
	 * @param methodName
	 *            Name of method to be invoked
	 * @param Instance
	 *            Instance of invoking class (this)
	 */
	protected void addListener(final Plugin Plugin, Object Instance, String methodName) {
		if (Instance instanceof mcbot) {
			addListener(Plugin, Instance, methodName, (mcbot) Instance);
		} else {
			addListener(Plugin, Instance, methodName, null);
		}
	}

	/**
	 * Adds listener to current menu item
	 * 
	 * @param Bot
	 *            Bot owner
	 * @param Instance
	 *            Instance of bot
	 * @param methodName
	 *            Name of method to be invoked
	 */
	protected void addListener(mcbot Bot, Object Instance, String methodName) {
		addListener(null, Instance, methodName, Bot);
	}

	/**
	 * Adds listener to current menu item.
	 * 
	 * @param Plugin
	 *            Instance of invoking class (this)
	 * @param methodName
	 *            Name of method to be invoked
	 * @param Bot
	 *            Bot to be included into event
	 * @param Instance
	 *            Instance of invoking class (this)
	 */
	protected void addListener(final Plugin Plugin, final Object Instance, String methodName, final mcbot Bot) {
		try {
			if (Instance.getClass().getDeclaredMethod(methodName, MenuActionEvent.class) != null) {
				final Method m = Instance.getClass().getDeclaredMethod(methodName, MenuActionEvent.class);
				item.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						try {
							if (Plugin == null) {
								if (Bot != null) {
									boolean ac = m.isAccessible();
									m.setAccessible(true);
									m.invoke(Instance, new MenuActionEvent(Bot.getCacheText(), item.getText(), item.isSelected(), Bot));
									m.setAccessible(ac);
								}
							} else {
								PluginInfo plinfo = storage.pluginManager.getPluginInfo(Plugin);
								boolean ac = m.isAccessible();
								m.setAccessible(true);
								m.invoke(Instance, new MenuActionEvent(plinfo, item.getText(), item.isSelected(), Bot));
								m.setAccessible(ac);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
