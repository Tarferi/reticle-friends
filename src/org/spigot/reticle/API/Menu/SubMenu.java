package org.spigot.reticle.API.Menu;

import javax.swing.JMenu;

public class SubMenu extends AbstractMenuItem {
	protected JMenu item;
	
	public String getItemName() {
		return item.getText();
	}
	
	public SubMenu(String name) {
		super(new JMenu(name));
		item=(JMenu) super.item;
		item.setEnabled(true);
	}
	
	public SubMenu(String name,boolean enabled) {
		super(new JMenu(name));
		item=(JMenu) super.item;
		item.setEnabled(enabled);
	}
	
	public void addItem(AbstractMenuItem item) {
		this.item.add(item.item);
	}
	
	public void addSeparator() {
		item.addSeparator();
	}
	
}
