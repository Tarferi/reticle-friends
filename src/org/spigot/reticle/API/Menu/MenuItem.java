package org.spigot.reticle.API.Menu;

import javax.swing.JMenuItem;

import org.spigot.reticle.API.Plugin;
import org.spigot.reticle.botfactory.mcbot;

public class MenuItem extends AbstractMenuItem {
	protected final JMenuItem item;

	public MenuItem(String name) {
		super(new JMenuItem(name));
		item = super.item;
		item.setEnabled(true);
	}

	public MenuItem(String name, Plugin Plugin, Object Instance, String methodName) {
		super(new JMenuItem(name));
		item = super.item;
		item.setEnabled(true);
		super.addListener(Plugin, Instance, methodName);
	}

	public MenuItem(String name, boolean enabled, Plugin Plugin, Object Instance, String methodName) {
		super(new JMenuItem(name));
		item = super.item;
		item.setEnabled(enabled);
		super.addListener(Plugin, Instance, methodName);
	}

	public MenuItem(String name, boolean enabled) {
		super(new JMenuItem(name));
		item = super.item;
		item.setEnabled(enabled);
	}

	public MenuItem(String name, mcbot Bot, Object Instance, String methodName) {
		super(new JMenuItem(name));
		item = super.item;
		super.addListener(Bot, Instance, methodName);
	}

	public MenuItem(String name, mcbot Bot, String methodName) {
		super(new JMenuItem(name));
		item = super.item;
		super.addListener(Bot, Bot, methodName);
	}

	public MenuItem(String name, boolean enabled, mcbot Instance, String methodName) {
		super(new JMenuItem(name));
		item = super.item;
		item.setEnabled(enabled);
		super.addListener(Instance, Instance, methodName);
	}
}
