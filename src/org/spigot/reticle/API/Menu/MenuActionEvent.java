package org.spigot.reticle.API.Menu;

import org.spigot.reticle.PluginInfo;
import org.spigot.reticle.botfactory.mcbot;

public class MenuActionEvent {
	private final String itemName;
	private final PluginInfo plinfo;
	private final boolean ischeck;
	private final mcbot bot;
	private final String sel;

	/**
	 * For CheckBoxMenuItem, returns True if item is checked
	 * 
	 * @return Returns True if item is checked
	 */
	public boolean isChecked() {
		return ischeck;
	}

	/**
	 * For context menu items
	 * 
	 * @return Returns selected text
	 */
	public String getSelection() {
		return sel;
	}

	/**
	 * Returns PluginInfo owned by plugin
	 * 
	 * @return Returns PluginInfo owned by plugin
	 */
	public PluginInfo getPluginInfo() {
		return plinfo;
	}

	/**
	 * Returns selected item name
	 * 
	 * @return Returns selected item name
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * Returns selected bot or null if no bot is selected
	 * 
	 * @return Returns selected bot or null if no bot is selected
	 */
	public mcbot getBot() {
		return bot;
	}

	protected MenuActionEvent(String sel, String itemName, boolean ischeck, mcbot bot) {
		this.itemName = itemName;
		this.plinfo = null;
		this.sel = sel;
		this.ischeck = ischeck;
		this.bot = bot;
	}

	protected MenuActionEvent(PluginInfo plinfo, String itemName, boolean ischeck, mcbot bot) {
		this.itemName = itemName;
		this.plinfo = plinfo;
		this.ischeck = ischeck;
		this.bot = bot;
		this.sel = null;
	}

}
