package org.spigot.reticle.API.Menu;

import javax.swing.JCheckBoxMenuItem;

import org.spigot.reticle.API.Plugin;
import org.spigot.reticle.botfactory.mcbot;

public class CheckBoxMenuItem extends AbstractMenuItem {
	protected JCheckBoxMenuItem item;

	public CheckBoxMenuItem(String name, boolean checked) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(true);
		item.setSelected(checked);
	}

	public CheckBoxMenuItem(String name, boolean checked, Plugin Plugin, Object Instance, String methodName) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(true);
		item.setSelected(checked);
		super.addListener(Plugin, Instance, methodName);
	}

	public CheckBoxMenuItem(String name, boolean checked, boolean enabled, Plugin Plugin, Object Instance, String methodName) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(enabled);
		item.setSelected(checked);
		super.addListener(Plugin, Instance, methodName);
	}

	public CheckBoxMenuItem(String name, boolean checked, boolean enabled) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(enabled);
		item.setSelected(checked);
	}

	public CheckBoxMenuItem(String name, boolean checked, mcbot Instance, String methodName) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(true);
		item.setSelected(checked);
		super.addListener(Instance, Instance, methodName);
	}

	public CheckBoxMenuItem(String name, boolean checked, Plugin Plugin, mcbot Instance, String methodName) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(true);
		item.setSelected(checked);
		super.addListener(Plugin, Instance, methodName);
	}

	public CheckBoxMenuItem(String name, boolean checked, boolean enabled, mcbot Instance, String methodName) {
		super(new JCheckBoxMenuItem(name));
		item = (JCheckBoxMenuItem) super.item;
		item.setEnabled(enabled);
		item.setSelected(checked);
		super.addListener(Instance, Instance, methodName);
	}
}
