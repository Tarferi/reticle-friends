package org.spigot.reticle.API;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.spigot.reticle.storage;
import org.spigot.reticle.botfactory.mcbot;

public class Plugin {
	/**
	 * Menu Handler
	 * 
	 * @param e
	 */
	/*
	 * public final void DisablePlugin(MenuActionEvent e) { String val =
	 * e.getItemName().toLowerCase(); if (e.getBot() != null) { if
	 * (val.equals("enabled on " + e.getBot().gettabname().toLowerCase())) {
	 * e.getBot().tryEnablePlugin(e); } } else if
	 * (val.equals("enable on all servers") ||
	 * (val.equals("disable on all servers"))) { HashMap<String, mcbot> bots =
	 * storage.getInstance().settin.bots; for (mcbot bott : bots.values()) {
	 * bott.tryEnablePlugin(e); } } else if (val.equals("unload plugin")) {
	 * storage.pluginManager.unloadPlugin(e.getPluginInfo()); } }
	 */
	/**
	 * Called when plugin is loaded (Before enabling)
	 */
	public void onLoad() {

	}
	
	public Logger getLogger() {
		return new Logger(this);
	}

	/**
	 * Called when all plugins are loaded
	 */
	public void onEnable() {

	}

	/**
	 * Called before plugin is unloaded
	 */
	public void onDisable() {

	}

	/**
	 * Returns List of all bots allowing plugin
	 * @param plugin
	 * @return Returns list of all bots allowing plugin
	 */
	public final List<mcbot> getAllowedServers(Plugin plugin) {
		String name=storage.pluginManager.getPluginInfo(plugin).Name;
		Collection<mcbot> bots = storage.getInstance().settin.bots.values();
		List<mcbot> allowed = new ArrayList<mcbot>();
		for (mcbot bot : bots) {
			if (bot.getAllowedPlugins().contains(name)) {
				allowed.add(bot);
			}
		}
		return allowed;
	}

	/**
	 * Never invoked directly
	 */
	public void onUnload() {
	}

	private final boolean isListener(Class<?> Class) {
		return Listener.class.isAssignableFrom(Class);
	}

	/**
	 * Register event listener
	 * 
	 * @param Plugin
	 *            Plugin object
	 * @param Instance
	 *            Instance of listener object
	 */
	protected final void addEventListener(Plugin Plugin, Object Instance) {
		Class<?> cls = Instance.getClass();
		if (isListener(cls)) {
			Method[] methods = cls.getMethods();
			String MasterClasses = "org.spigot.reticle.events.";
			for (Method method : methods) {
				if (!method.isAnnotationPresent(EventHandler.class)) {
					continue;
				}
				Class<?>[] types = method.getParameterTypes();
				if (types.length == 1) {
					Class<?> type = types[0];
					try {
						// if(type.isInstance(Event.class)) {}
						if (type.getName().toLowerCase().startsWith(MasterClasses)) {
							if (Class.forName(type.getName()) != null) {
								storage.pluginManager.addMethod(Plugin, method, type, Instance);
							}
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}

				}
			}
		}
	}
}
