package org.spigot.reticle.API;

import java.io.File;
import java.io.IOException;

import org.ho.yaml.Yaml;
import org.spigot.reticle.PluginInfo;
import org.spigot.reticle.storage;

/**
 * Config handler. This object should make it easier to manage config
 * 
 * @author Encorn
 * 
 */
public class YAMLConfigHandler {
	private final PluginInfo plinfo;
	private final String ConfigFile;

	public YAMLConfigHandler(Plugin plugin) {
		this.plinfo = storage.pluginManager.getPluginInfo(plugin);
		this.ConfigFile = "Plugins/" + plinfo.Name + "/config.yml";
	}

	/**
	 * Reads config stored in file
	 * 
	 * @return Returns object stored in file or null
	 */
	public Object ReadConfig(Plugin pl, Class<? extends Object> Class) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Object o = null;
		try {
			o = Yaml.loadType(getFile(), Class);
		} catch (Exception e) {
		}
		return o;
	}

	/**
	 * Writes config object to file
	 * 
	 * @param ConfigObject
	 *            The object to be written
	 */
	public void WriteConfig(Object ConfigObject) {
		try {
			init();
			File file = getFile();
			if (file == null) {
				System.err.println("Failed to create config file");
			} else {
				Yaml.dump(ConfigObject, file, true);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Failed to write config file");
		}
	}

	private File getFile() {
		if (init()) {
			return new File(storage.CurrentDir + ConfigFile);
		} else {
			return null;
		}
	}

	/**
	 * Creates plugin config.yml file in plugin folder
	 * 
	 * @return True if successful, False if otherwise
	 */
	public boolean init() {
		if (mde("Plugins")) {
			if (mde("Plugins/" + plinfo.Name)) {
				if (mfe(ConfigFile)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean mfe(String filename) {
		File f = new File(storage.CurrentDir + filename);
		if (f.exists()) {
			if (f.isFile()) {
				return true;
			}
		}
		try {
			return (f.createNewFile());
		} catch (IOException e) {
			return false;
		}
	}

	private boolean mde(String dirname) {
		File f = new File(storage.CurrentDir + dirname);
		if (f.exists()) {
			if (f.isDirectory()) {
				return true;
			}
		}
		return (f.mkdirs());
	}

}
