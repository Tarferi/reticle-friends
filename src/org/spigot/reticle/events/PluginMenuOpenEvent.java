package org.spigot.reticle.events;

import java.util.LinkedHashMap;

import org.spigot.reticle.storage;
import org.spigot.reticle.API.Plugin;
import org.spigot.reticle.API.Menu.AbstractMenuItem;
import org.spigot.reticle.botfactory.mcbot;

/**
 * Fired when collecting plugin's custom menus
 * @author Encorn
 *
 */
public class PluginMenuOpenEvent extends Event {
	private LinkedHashMap<String, LinkedHashMap<String, AbstractMenuItem>> items;

	public PluginMenuOpenEvent(LinkedHashMap<String, LinkedHashMap<String, AbstractMenuItem>> it) {
		super(null);
		this.items = it;
	}

	public PluginMenuOpenEvent(mcbot mcbot, LinkedHashMap<String, LinkedHashMap<String, AbstractMenuItem>> it) {
		super(mcbot);
		this.items = it;
	}

	/**
	 * Add item to plugin menu
	 * 
	 * @param Plugin
	 *            Plugin to handle
	 * @param item
	 *            Item to add
	 */
	public void addItem(Plugin Plugin, AbstractMenuItem item) {
		String plname = storage.pluginManager.getPluginInfo(Plugin).Name;
		if (!items.containsKey(plname)) {
			items.put(plname, new LinkedHashMap<String, AbstractMenuItem>());
		}
		this.items.get(plname).put(item.getItemName(), item);
	}

}
