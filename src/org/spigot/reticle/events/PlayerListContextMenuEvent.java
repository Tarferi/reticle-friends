package org.spigot.reticle.events;

import java.util.LinkedHashMap;

import org.spigot.reticle.API.Menu.AbstractMenuItem;
import org.spigot.reticle.botfactory.mcbot;

/**
 * Fired when user right clicks on Player list item
 * 
 * @author Encorn
 * 
 */
public class PlayerListContextMenuEvent extends Event {
	private LinkedHashMap<String, AbstractMenuItem> map = new LinkedHashMap<String, AbstractMenuItem>();
	private final String text;

	public PlayerListContextMenuEvent(LinkedHashMap<String, AbstractMenuItem> mapping, mcbot bot, String text) {
		super(bot);
		this.map = mapping;
		this.text = text;
	}

	/**
	 * Returns clicked text
	 * 
	 * @return Returns clicked text
	 */
	public String getClickedText() {
		return text;
	}

	/**
	 * @param item
	 *            Item to be added
	 * @return Returns True if successful, False if name already exists
	 */
	public boolean addEntry(AbstractMenuItem item) {
		if (map.containsKey(item.getItemName())) {
			return false;
		}
		map.put(item.getItemName(), item);
		return true;
	}

}
