package org.spigot.reticle.events;

import java.util.LinkedHashMap;

import org.spigot.reticle.API.Menu.AbstractMenuItem;
import org.spigot.reticle.botfactory.mcbot;

/**
 * Fired when user right clicks on chat log
 * 
 * @author Encorn
 * 
 */
public class ChatLogContextMenuEvent extends Event {

	private LinkedHashMap<String, AbstractMenuItem> map = new LinkedHashMap<String, AbstractMenuItem>();
	private final String text;

	public ChatLogContextMenuEvent(LinkedHashMap<String, AbstractMenuItem> mapping, mcbot bot, String text) {
		super(bot);
		this.map = mapping;
		this.text = text;
	}

	/**
	 * Returns selected text
	 * 
	 * @return Returns selected text
	 */
	public String getClickedText() {
		return text;
	}

	/**
	 * Adds item to menu
	 * 
	 * @param item
	 *            Item to be added
	 * @return True if successful, False if otherwise
	 */
	public boolean addEntry(AbstractMenuItem item) {
		if (map.containsKey(item)) {
			return false;
		}
		try {
			map.put(item.getItemName(), item);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
