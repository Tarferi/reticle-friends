package org.spigot.reticle.events;

import java.util.HashMap;

import org.spigot.reticle.storage;
import org.spigot.reticle.API.Menu.AbstractMenuItem;
import org.spigot.reticle.botfactory.mcbot;

/**
 * Invoked when user right-clicks bot tab
 * @author Encorn
 *
 */
public class BotContextMenuEvent extends Event {
	private HashMap<String, AbstractMenuItem> items = new HashMap<String, AbstractMenuItem>();
	private final String text;

	public BotContextMenuEvent(mcbot bot, HashMap<String, AbstractMenuItem> hash, String text) {
		super(bot);
		this.items = hash;
		this.text = text;
	}

	/**
	 * Returns selected bot tab name
	 * 
	 * @return Returns selected bot tab name
	 */
	public String getClickedBotTabName() {
		return text;
	}

	/**
	 * Returns selected bot
	 * 
	 * @return Returns selected bot
	 */
	public mcbot getClickedBot() {
		return storage.getInstance().settin.bots.get(text);
	}

	/**
	 * @param item
	 *            Menu item
	 * @param bot
	 *            Bot owner
	 * @return Returns True if successful, False if name already exists or
	 *         method is not found
	 */
	public boolean addEntry(AbstractMenuItem item) {
		if (items.containsKey(item)) {
			return false;
		}
		try {
			items.put(item.getItemName(), item);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
