package org.spigot.reticle.botfactory;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.commons.io.FileUtils;
import org.spigot.reticle.PluginInfo;
import org.spigot.reticle.PluginManager;
import org.spigot.reticle.storage;
import org.spigot.reticle.API.Plugin;

public class PluginUpdater {

	private class JTableButtonMouseListener extends MouseAdapter {

		@Override
		public void mouseClicked(MouseEvent e) {
			JTable table = (JTable) e.getSource();
			int column = table.getColumnModel().getColumnIndexAtX(e.getX());
			int totals = 0;
			int totalsy = e.getY();
			int row, o;
			for (row = 0, o = table.getRowCount(); row < o; row++) {
				int t = table.getRowHeight(row);
				if (totalsy > totals && totalsy < totals + t) {
					break;
				}
				totals += t;
			}
			if (row < table.getRowCount() && row >= 0 && column < table.getColumnCount() && column >= 0) {
				Object value = table.getValueAt(row, column);
				if (value instanceof JButton) {
					((JButton) value).doClick();
				}
			}
		}
	}

	private class JTableButtonRenderer implements TableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			if (value instanceof JButton) {
				JButton button = (JButton) value;
				if (isSelected) {
					button.setForeground(table.getSelectionForeground());
					button.setBackground(table.getSelectionBackground());
				} else {
					button.setForeground(table.getForeground());
					button.setBackground(UIManager.getColor("Button.background"));
				}
				return button;
			} else {
				return (Component) value;
			}
		}
	}

	private class PluginUpdateStruct {
		protected final PluginInfo plugin;
		protected final JButton button;

		private PluginUpdateStruct(PluginInfo plugin, JButton button) {
			this.plugin = plugin;
			this.button = button;
		}
	}

	protected static enum PluginListings {
		NEW, UPDATE, INSTALLED
	}

	protected PluginUpdater() {
		storage.conlog("Available plugins:");
	}

	protected boolean PluginUpdate(JTextPane chatlog, PluginListings[] lists, mcbot bot) {
		List<PluginInfo> plugins = PluginManager.getOnlinePluginList();
		List<PluginUpdateStruct> tableitems = new ArrayList<PluginUpdateStruct>();
		List<PluginListings> ar = Arrays.asList(lists);
		boolean install = ar.contains(PluginListings.NEW);
		boolean update = ar.contains(PluginListings.UPDATE);
		boolean installed = ar.contains(PluginListings.INSTALLED);
		if (plugins != null) {
			List<String> plugnames = storage.pluginManager.getAllPluginNames();
			for (final PluginInfo plugin : plugins) {
				JButton dl = null;
				if (!plugnames.contains(plugin.Name) && install) {
					dl = new JButton("Install");
				} else {
					Plugin local = storage.pluginManager.getPluginByName(plugin.Name);
					if (local != null) {
						String version = storage.pluginManager.getPluginInfo(local).Version;
						if (!version.equals(plugin.Version)) {
							if (update) {
								dl = new JButton("Update");
							}
						} else {
							if (installed) {
								dl = new JButton("Installed");
								dl.setEnabled(false);
							}
						}
					}
				}
				if (dl != null) {
					dl.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent arg0) {
							((JButton) arg0.getSource()).setEnabled(false);
							plugininstaller inst = new plugininstaller(plugin);
							inst.start();
						}
					});
					tableitems.add(new PluginUpdateStruct(plugin, dl));
				}
			}
			if (tableitems.size() > 0) {
				JTable table = new JTable();
				String[] columnNames = new String[] { "Name", "Version", "Author", "Description", "Install" };
				Object[][] data = new Object[tableitems.size() + 1][4];
				JButton dis = new JButton("TEST");
				dis.setEnabled(false);
				data[0] = new Object[] { "Name", "Version", "Author", "Description", dis };
				for (int i = 0, o = tableitems.size(); i < o; i++) {
					PluginInfo inf = tableitems.get(i).plugin;
					data[i + 1] = new Object[] { inf.Name, inf.Version, inf.Author, inf.desc, tableitems.get(i).button };
				}
				table.setModel(new DefaultTableModel(data, columnNames) {
					private static final long serialVersionUID = 1L;

					@Override
					public boolean isCellEditable(int row, int col) {
						return false;
					}
				});
				table.setColumnSelectionAllowed(false);
				table.setRowSelectionAllowed(false);
				table.getColumnModel().getColumn(4).setCellRenderer(new JTableButtonRenderer());
				table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				table.setFillsViewportHeight(true);
				update(table);
				dis.setText("");
				DefaultTableCellRenderer centerRenderer = new MyTableCellRenderer();
				centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
				table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
				table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
				table.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
				table.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

				table.addMouseListener(new JTableButtonMouseListener());
				chatlog.insertComponent(table);
				bot.logmsg("  \n", false);
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	private class MyTableCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {
		private static final long serialVersionUID = 1L;

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			setBackground(null);
			super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			setText(String.valueOf(value));
			if (row == 0) {
				this.setText("<html><b>" + this.getText() + "</b></html>");
				setBackground(Color.ORANGE);
			}
			return this;
		}
	}

	private void update(JTable mainTable) {
		for (int i = 0; i < mainTable.getColumnCount(); i++) {
			adjustColumnSizes(mainTable, i, 2);
		}
		adjustJTableRowSizes(mainTable);
	}

	private void adjustJTableRowSizes(JTable jTable) {
		for (int row = 0; row < jTable.getRowCount(); row++) {
			int maxHeight = 0;
			for (int column = 0; column < jTable.getColumnCount(); column++) {
				TableCellRenderer cellRenderer = jTable.getCellRenderer(row, column);
				Object valueAt = jTable.getValueAt(row, column);
				Component tableCellRendererComponent = cellRenderer.getTableCellRendererComponent(jTable, valueAt, false, false, row, column);
				int heightPreferable = tableCellRendererComponent.getPreferredSize().height;
				maxHeight = Math.max(heightPreferable, maxHeight);
			}
			jTable.setRowHeight(row, maxHeight);
		}

	}

	private void adjustColumnSizes(JTable table, int column, int margin) {
		DefaultTableColumnModel colModel = (DefaultTableColumnModel) table.getColumnModel();
		TableColumn col = colModel.getColumn(column);
		int width;

		TableCellRenderer renderer = col.getHeaderRenderer();
		if (renderer == null) {
			renderer = table.getTableHeader().getDefaultRenderer();
		}
		Component comp = renderer.getTableCellRendererComponent(table, col.getHeaderValue(), false, false, 0, 0);
		width = comp.getPreferredSize().width;

		for (int r = 0; r < table.getRowCount(); r++) {
			renderer = table.getCellRenderer(r, column);
			comp = renderer.getTableCellRendererComponent(table, table.getValueAt(r, column), false, false, r, column);
			int currentWidth = comp.getPreferredSize().width;
			width = Math.max(width, currentWidth);
		}

		width += 2 * margin;

		col.setPreferredWidth(width);
		col.setWidth(width);
	}

	private class plugininstaller extends Thread {
		private PluginInfo plugin;

		private plugininstaller(PluginInfo pl) {
			plugin = pl;
		}

		@Override
		public void run() {
			Plugin local = storage.pluginManager.getPluginByName(plugin.Name);
			if (local != null) {
				storage.pluginManager.unloadPlugin(local);
			}
			String file = "plugins/" + plugin.FileName.getName();
			String url = storage.pluginFilesURL + plugin.FileName.getName();
			try {
				storage.conlog("Installing plugin " + plugin.Name + "...");
				getfilefromurl(url, file);
				storage.conlog("§bSuccessfully installed plugin " + plugin.Name);
				storage.pluginManager.loadPlugin(plugin.FileName.getName());
			} catch (IOException e) {
				storage.conlog("§4Failed to install plugin " + plugin.Name);
			}
		}

		private void getfilefromurl(String url, String destfile) throws IOException {
			FileUtils.copyURLToFile(new URL(url), new File(destfile));
		}
	}
}
