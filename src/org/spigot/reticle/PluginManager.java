package org.spigot.reticle;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.spigot.reticle.API.POST;
import org.spigot.reticle.API.POST.POSTMETHOD;
import org.spigot.reticle.API.Plugin;
import org.spigot.reticle.API.ReticleThread;
import org.spigot.reticle.events.ConsoleCommandEvent;
import org.spigot.reticle.events.Event;
import org.spigot.reticle.events.PluginMenuOpenEvent;

public class PluginManager {
	private HashMap<Plugin, PluginInfo> Plugins = new HashMap<Plugin, PluginInfo>();
	private HashMap<Class<?>, HashMap<Plugin, HashMap<Method, Object>>> methods_by_plugins = new HashMap<Class<?>, HashMap<Plugin, HashMap<Method, Object>>>();
	private HashMap<Plugin, List<ReticleThread>> Threads = new HashMap<Plugin, List<ReticleThread>>();

	protected void addThread(Plugin Plugin, ReticleThread Thread) {
		if (!Threads.containsKey(Plugin)) {
			Threads.put(Plugin, new ArrayList<ReticleThread>());
		}
		Threads.get(Plugin).add(Thread);
	}

	protected void killThreadsForPlugin(Plugin Plugin) {
		if (Threads.containsKey(Plugin)) {
			for (ReticleThread Th : Threads.get(Plugin)) {
				try {
					Th.stop();
				} catch (Exception e) {
				}
				removeThread(Plugin, Th);
			}
		}
	}

	protected void removeThread(Plugin Plugin, ReticleThread Thread) {
		if (Threads.containsKey(Plugin)) {
			if (Threads.get(Plugin).contains(Thread)) {
				Threads.get(Plugin).remove(Thread);
				if (Threads.get(Plugin).isEmpty()) {
					Threads.remove(Plugin);
				}
			}
		}
	}

	protected PluginManager() {

	}

	/**
	 * Returns list of names of all plugins
	 * 
	 * @return Returns list of names of all plugins
	 */
	public List<String> getAllPluginNames() {
		Object[] pl = this.getPluginInfos().toArray();
		List<String> res = new ArrayList<String>();
		for (Object plinfo : pl) {
			res.add(((PluginInfo) plinfo).Name);
		}
		return res;
	}

	/**
	 * @param Plugin
	 *            The plugin
	 * @return PluginInfo for selected plugin
	 */
	public PluginInfo getPluginInfo(Plugin Plugin) {
		return Plugins.get(Plugin);
	}

	/**
	 * Finds plugin based on its file name
	 * 
	 * @param pl
	 * @return plugin
	 */
	public Plugin getPluginByFileName(String pl) {
		for (PluginInfo info : Plugins.values()) {
			if (info.FileName.getName().equalsIgnoreCase(pl) || info.FileName.getName().equalsIgnoreCase(pl + ".jar")) {
				return info.getInstance();
			}
		}
		return null;
	}

	/**
	 * Finds plugin specified by name
	 * 
	 * @param PluginName
	 * @return Plugin or null if not found
	 */
	public Plugin getPluginByName(String PluginName) {
		for (PluginInfo info : Plugins.values()) {
			if (info.Name.equalsIgnoreCase(PluginName)) {
				return info.getInstance();
			}
		}
		return null;
	}

	/**
	 * Right before bot app is closed
	 */
	protected void unloadAllPlugins() {
		Object[] Pluginss = Plugins.keySet().toArray();
		for (Object pl : Pluginss) {
			unloadPlugin((Plugin) pl);
		}
	}

	public void DisablePlugin(PluginInfo PluginInfo) {
		PluginInfo.Disable();
	}

	public void EnablePlugin(PluginInfo PluginInfo) {
		PluginInfo.Enable();
	}

	public void unloadPlugin(PluginInfo PluginInfo) {
		unloadPlugin(PluginInfo.getInstance());
	}

	/**
	 * Unloads plugin
	 * 
	 * @param Plugin
	 *            Plugin to be unloaded
	 */
	public void unloadPlugin(Plugin Plugin) {
		if (this.pluginExists(Plugin)) {
			Plugin.onDisable();
			Plugin.onUnload();
			this.RemoveAllMethodsForPlugin(Plugin);
			PluginInfo plinfo = Plugins.get(Plugin);
			plinfo.closeLoader();
			Plugins.remove(Plugin);
		}
	}

	/**
	 * Parse all enabled plugins
	 * 
	 * @return Array of loaded plugins
	 */
	public Collection<PluginInfo> getPluginInfos() {
		return Plugins.values();
	}

	private void registerPlugin(PluginInfo plug) {
		Plugin pl = plug.getInstance();
		if (!pluginExists(pl) && pl != null) {
			Plugins.put(pl, plug);
			pl.onLoad();
		}
	}

	private void enablePlugins() {
		for (Plugin pl : Plugins.keySet()) {
			enablePlugin(pl);
		}
	}

	private void enablePlugin(Plugin pl) {
		PluginInfo plinfo = Plugins.get(pl);
		storage.conlog("�bEnabling �f" + plinfo.Name + "�b version �f" + plinfo.Version + " �bmade by �f" + plinfo.Author);
		pl.onEnable();
	}

	public boolean loadPlugin(String filename) {
		PluginInfo pl = tryLoadPlugin(new File(storage.CurrentDir + "plugins/" + filename));
		if (pl == null) {
			pl = tryLoadPlugin(new File(storage.CurrentDir + "plugins/" + filename + ".jar"));
		}
		if (pl == null) {
			return false;
		} else {
			registerPlugin(pl);
			enablePlugin(pl.getInstance());
			return true;
		}
	}

	private PluginInfo tryLoadPlugin(File fileEntry) {
		try {
			ClassLoader currentThreadClassLoader = Thread.currentThread().getContextClassLoader();
			URLClassLoader ClazzL = new URLClassLoader(new URL[] { fileEntry.toURI().toURL() }, currentThreadClassLoader);
			BufferedReader in = new BufferedReader(new InputStreamReader(ClazzL.getResourceAsStream("plugin.yml")));
			String author = null;
			String name = null;
			String main = null;
			String version = null;
			String desc = null;
			while (in.ready()) {
				String s = in.readLine();
				String opt = s.split(": ")[0].toLowerCase();
				String param = s.substring(opt.length() + 2);
				if (opt.equals("author")) {
					author = param;
				} else if (opt.equals("name")) {
					name = param;
				} else if (opt.equals("main")) {
					main = param;
				} else if (opt.equals("version")) {
					version = param;
				} else if (opt.equals("description")) {
					desc = param;
				}
			}
			in.close();
			if (main == null || author == null || name == null || version == null) {
				ClazzL.close();
				return null;
			}
			if (this.getPluginByName(name) != null) {
				ClazzL.close();
				return null;
			}
			Class<?> c = ClazzL.loadClass(main);
			if (desc == null) {
				desc = "???";
			}
			return new PluginInfo(ClazzL, author, fileEntry, version, name, desc, c);
		} catch (Exception e) {
			return null;
		}
	}

	protected void loadAllPlugins() {
		String pluginsdir = "plugins";
		File pldir = new File(storage.CurrentDir + pluginsdir);
		if (!pldir.exists() || !pldir.isDirectory()) {
			if (!pldir.mkdirs()) {
				storage.conlog("�4Fatal error while loading plugins");
				return;
			}
		}
		for (File fileEntry : pldir.listFiles()) {
			if (!fileEntry.isDirectory()) {
				PluginInfo plug = tryLoadPlugin(fileEntry);
				if (plug != null) {
					storage.conlog("�2Loaded plugin " + plug.FileName);
					registerPlugin(plug);
				} else {
					storage.conlog("�4Failed to load plugin " + fileEntry.getPath());
				}
			}
		}
		enablePlugins();
	}

	protected boolean pluginExists(Plugin Plugin) {
		return Plugins.containsKey(Plugin);
	}

	protected boolean pluginHasMethod(Plugin Plugin, Method Method, Class<?> Class) {
		if (methods_by_plugins.containsKey(Class)) {
			if (methods_by_plugins.get(Class).containsKey(Plugin)) {
				if (methods_by_plugins.get(Class).get(Plugin).containsKey(Method)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	protected boolean pluginIsHandled(Plugin Plugin, Class<?> Class) {
		if (methods_by_plugins.containsKey(Class)) {
			return methods_by_plugins.get(Class).containsKey(Plugin);
		} else {
			return false;
		}
	}

	protected boolean ClassIsHandled(Class<?> Class) {
		return methods_by_plugins.containsKey(Class);
	}

	/**
	 * Invoked when event is being dispatched to enabled listeners
	 * 
	 * @param e
	 *            Event to be dispatcher
	 * @param list
	 *            List of enabled plugins
	 */
	public void invokeEvent(Event e, List<String> list) {
		invokeEvent(e, list, false);
	}

	public void invokeEvent(PluginMenuOpenEvent e) {
		invokeEvent(e, new ArrayList<String>(), true);
	}

	/**
	 * Invoken when event is being dispatched to listeners
	 * 
	 * @param event
	 *            Event to be dispatched
	 * @param override
	 *            If True, all plugins will receive the event
	 */
	public void invokeEvent(ConsoleCommandEvent event, boolean override) {
		invokeEvent(event, null, true);
	}

	private void invokeEvent(Event e, List<String> list, boolean override) {
		Class<?> cls = e.getClass();
		if (ClassIsHandled(cls)) {
			for (Plugin plugin : methods_by_plugins.get(cls).keySet()) {
				if (override || list != null) {
					if (override || list.contains(Plugins.get(plugin).Name)) {
						Set<Method> methods = methods_by_plugins.get(cls).get(plugin).keySet();
						for (Method method : methods) {
							try {
								Object instance = methods_by_plugins.get(cls).get(plugin).get(method);
								method.invoke(instance, e);
							} catch (Exception e1) {
							}
						}
					}
				}
			}
		}
	}

	private void RemoveAllMethodsForPlugin(Plugin pl) {
		for (Class<?> cls : methods_by_plugins.keySet()) {
			if (methods_by_plugins.get(cls).containsKey(pl)) {
				methods_by_plugins.get(cls).remove(pl);
			}
		}
	}

	/**
	 * Called when method is added to dispatcher
	 * 
	 * @param Plugin
	 * @param Method
	 * @param Class
	 * @param Instance
	 */
	public void addMethod(Plugin Plugin, Method Method, Class<?> Class, Object Instance) {
		if (!methods_by_plugins.containsKey(Class)) {
			methods_by_plugins.put(Class, new HashMap<Plugin, HashMap<Method, Object>>());
		}
		if (!methods_by_plugins.get(Class).containsKey(Plugin)) {
			methods_by_plugins.get(Class).put(Plugin, new HashMap<Method, Object>());
		}
		if (!methods_by_plugins.get(Class).get(Plugin).containsKey(Method)) {
			methods_by_plugins.get(Class).get(Plugin).put(Method, Instance);
		}
	}

	public static List<PluginInfo> getOnlinePluginList() {
		POST form = new POST(storage.pluginURL, true);
		form.setMethod(POSTMETHOD.GET);
		if (form.Execute()) {
			String text = form.getResponse();
			String delim1 = Character.toString ((char) 4);
			String delim2 = Character.toString ((char) 5);
			String delim3 = Character.toString ((char) 6);
			if (text.contains(delim1)) {
				if (text.contains(delim2)) {
					String[] total = text.split(delim1);
					List<PluginInfo> plugins = new ArrayList<PluginInfo>();
					for (String t : total) {
						String[] plug = t.split(delim2);
						String name = "???", author = "???", ver = "???", desc = "???", file = "???";
						for (int i = 0, o = plug.length; i < o; i++) {
							String[] vals=plug[i].split(delim3);
							String key = vals[0];
							String val = vals[1];
							switch (key) {
								case "name":
									name = val;
								break;
								case "author":
									author = val;
								break;
								case "ver":
									ver = val;
								break;
								case "desc":
									desc = val;
								break;
								case "file":
									file = val;
								break;
							}
						}
						PluginInfo pl = new PluginInfo(null, author, new File(file), ver, name, desc, null);
						plugins.add(pl);
					}
					return plugins;
				}
			}
			return null;
		} else {
			storage.conlog("�4Plugin service is not available");
			return null;
		}
	}

}
