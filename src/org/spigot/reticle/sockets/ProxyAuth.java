package org.spigot.reticle.sockets;

import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.List;

public class ProxyAuth {
	
	private static String	hostName;
	private static String	proxyUser;
	private static String	proxyPass;
	private static Integer	hostPort;
	
	@SuppressWarnings("static-access")
	public ProxyAuth(String hostName, int hostPort, String proxyUser, String proxyPass) {
		this.hostName = hostName;
		this.hostPort = hostPort;
		this.proxyUser = proxyUser;
		if (stringHasValue(proxyPass))
			proxyPass = new org.spigot.reticle.EncryptUtils("||_|;Reticle&^R]Fri3nds|V.<>^^*(").decrypt(proxyPass);
		this.proxyPass = proxyPass;
	}
	
	/**
	 * 
	 * @return Authenticated Proxy (With Detected type; HTTP, or SOCKS 4/5)
	 */
	public static Proxy p() {
		Proxy proxy = Proxy.NO_PROXY;
		if (hostName != null) {
			try
			{
				InetSocketAddress address = new InetSocketAddress(hostName, hostPort);
				proxy = new Proxy(detectProxyType(address), address);
			} catch (Exception ignored) {
				ignored.printStackTrace();
			}
		}
		PasswordAuthentication passwordAuthentication = null;
		if ((!proxy.equals(Proxy.NO_PROXY)) && (stringHasValue(proxyUser)) && (stringHasValue(proxyPass)))
		{
			auth(passwordAuthentication, proxy);
		}
		return proxy;
	}
	
	/**
	 * Authenticates proxy
	 * 
	 * @param passwordAuthentication
	 * @param proxy
	 */
	private static void auth(PasswordAuthentication passwordAuthentication, Proxy proxy) {
		passwordAuthentication = new PasswordAuthentication(proxyUser, proxyPass.toCharArray());
		
		final PasswordAuthentication auth = passwordAuthentication;
		Authenticator.setDefault(new Authenticator()
		{
			@Override
			protected PasswordAuthentication getPasswordAuthentication()
			{
				return auth;
			}
		});
	}
	
	/**
	 * Determines of string is null or empty
	 * 
	 * @param string
	 * @return
	 */
	public final static boolean stringHasValue(String string) {
		return (string != null) && (!string.isEmpty());
	}
	
	/**
	 * Detects Proxy Type
	 * 
	 * @param proxyAddress
	 * @return
	 * @throws IOException
	 */
	private static Proxy.Type detectProxyType(InetSocketAddress proxyAddress)
			throws IOException
	{
		URL url = new URL("http://127.0.0.1");
		List<Proxy.Type> proxyTypesToTry = Arrays.asList(Proxy.Type.SOCKS, Proxy.Type.HTTP);
		
		for (Proxy.Type proxyType : proxyTypesToTry)
		{
			PasswordAuthentication passwordAuthentication = null;
			Proxy proxy = new Proxy(proxyType, proxyAddress);
			// Try with SOCKS
			URLConnection connection = null;
			try
			{
				if ((!proxy.equals(Proxy.NO_PROXY)) && (stringHasValue(proxyUser)) && (stringHasValue(proxyPass)))
				{
					auth(passwordAuthentication, proxy);
				}
				connection = url.openConnection(proxy);
				
				// Can modify timeouts if default timeout is taking too long
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(5000);
				
				connection.getContent();
				
				// If we get here we made a successful connection
				return (proxyType);
			} catch (SocketException e) // or possibly more generic IOException?
			{
				if (e.getMessage().equalsIgnoreCase("SOCKS: Connection not allowed by ruleset"))
					return Proxy.Type.SOCKS;
				e.printStackTrace();
			}
		}
		
		// No proxies worked if we get here
		return (Proxy.Type.DIRECT);
	}
}
