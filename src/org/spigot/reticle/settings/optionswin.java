package org.spigot.reticle.settings;

import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.spigot.reticle.storage;

public class optionswin extends JDialog {
	
	private static final long	serialVersionUID	= 1L;
	private JTextField			userField;
	private JTextField			passwordField;
	private JTextField			portField;
	private JTextField			hostField;
	
	public optionswin() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				storage.closeoptionswin();
			}
		});
		getContentPane().setLayout(null);
		setBounds(100, 100, 365, 375);
		
		final JPanel content = new JPanel();
		content.setBounds(0, 0, 375, 317);
		getContentPane().add(content);
		content.setLayout(null);
		
		final JLabel llblUpdates = new JLabel("Check for updates after start: ", SwingConstants.RIGHT);
		llblUpdates.setBounds(9, 16, 320, 15);
		content.add(llblUpdates);
		
		final JCheckBox chckUpdates = new JCheckBox("");
		chckUpdates.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckUpdates.setBounds(329, 11, 21, 25);
		content.add(chckUpdates);
		
		final JLabel lblDebug = new JLabel("Send usage and error reports automatically: ", SwingConstants.LEFT);
		lblDebug.setBounds(9, 36, 353, 15);
		content.add(lblDebug);
		
		final JCheckBox chckDebug = new JCheckBox("");
		chckDebug.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckDebug.setBounds(329, 31, 21, 25);
		content.add(chckDebug);
		
		final JLabel lblPlugins = new JLabel("Load plugins automatically: ", SwingConstants.RIGHT);
		lblPlugins.setBounds(9, 56, 320, 15);
		content.add(lblPlugins);
		
		final JCheckBox chckPlugins = new JCheckBox("");
		chckPlugins.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckPlugins.setBounds(329, 51, 21, 25);
		content.add(chckPlugins);
		
		final JLabel lblSupport = new JLabel("Allow support server: ", SwingConstants.RIGHT);
		lblSupport.setBounds(9, 76, 320, 15);
		content.add(lblSupport);
		
		final JCheckBox chckSupport = new JCheckBox("");
		chckSupport.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckSupport.setBounds(329, 71, 21, 25);
		content.add(chckSupport);
		
		final JLabel lblSLogger = new JLabel("Logger enabled for special tabs: ", SwingConstants.RIGHT);
		lblSLogger.setBounds(9, 96, 320, 15);
		content.add(lblSLogger);
		
		final JCheckBox chckSLogger = new JCheckBox("");
		chckSLogger.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckSLogger.setBounds(329, 91, 21, 25);
		content.add(chckSLogger);
		
		final JLabel lblMainP = new JLabel("Enable main proxy: ", SwingConstants.RIGHT);
		lblMainP.setBounds(9, 116, 320, 15);
		content.add(lblMainP);
		
		final JCheckBox chckMainP = new JCheckBox("");
		chckMainP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckMainP.setBounds(329, 111, 21, 25);
		content.add(chckMainP);
		
		final JLabel lblPHost = new JLabel("Proxy Host:");
		lblPHost.setBounds(9, 169, 96, 15);
		content.add(lblPHost);
		
		final JLabel lblPPort = new JLabel("Proxy Port:");
		lblPPort.setBounds(9, 191, 96, 15);
		content.add(lblPPort);
		
		hostField = new JTextField();
		hostField.setBounds(97, 167, 253, 19);
		content.add(hostField);
		
		portField = new JTextField();
		portField.setBounds(97, 189, 253, 19);
		content.add(portField);
		
		final JLabel lblAuth = new JLabel("Proxy authentication: ", SwingConstants.RIGHT);
		lblAuth.setBounds(9, 220, 320, 15);
		content.add(lblAuth);
		
		final JCheckBox chckPAuth = new JCheckBox("");
		chckPAuth.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		chckPAuth.setBounds(329, 215, 21, 25);
		content.add(chckPAuth);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(10);
		passwordField.setBounds(97, 269, 253, 19);
		content.add(passwordField);
		
		final JLabel lblUser = new JLabel("Proxy User:");
		lblUser.setBounds(9, 249, 96, 15);
		content.add(lblUser);
		
		final JLabel lblPass = new JLabel("Proxy Pass:");
		lblPass.setBounds(9, 271, 96, 15);
		content.add(lblPass);
		
		userField = new JTextField();
		userField.setColumns(10);
		userField.setBounds(97, 247, 253, 19);
		content.add(userField);
		
		final JPanel panel = new JPanel();
		panel.setBounds(0, 315, 375, 35);
		getContentPane().add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		final JButton btnOk = new JButton("Ok");
		btnOk.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panel.add(btnOk);
		btnOk.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				HashMap<String, String> set = new HashMap<String, String>();
				set.put("autoupdate", chckUpdates.isSelected() + "");
				set.put("autosenddebug", chckDebug.isSelected() + "");
				set.put("loadplugins", chckPlugins.isSelected() + "");
				set.put("support", chckSupport.isSelected() + "");
				set.put("supportnick", storage.getSupportNick());
				set.put("speciallogger", chckSLogger.isSelected() + "");
				set.put("mProxy", chckMainP.isSelected() + "");
				set.put("mHost", hostField.getText());
				set.put("mPort", portField.getText());
				set.put("mAuth", chckPAuth.isSelected() + "");
				set.put("mUser", userField.getText());
				set.put("mPass", new org.spigot.reticle.EncryptUtils("||_|;Reticle&^R]Fri3nds|V.<>^^*(").encrypt(passwordField.getText()));
				storage.setglobalsettings(set);
				storage.savesettings();
				storage.closeoptionswin();
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		panel.add(btnCancel);
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				storage.closeoptionswin();
			}
		});
		
		chckMainP.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (chckMainP.isSelected()) {
					lblPHost.setEnabled(true);
					lblPPort.setEnabled(true);
					hostField.setEnabled(true);
					portField.setEnabled(true);
					chckPAuth.setEnabled(true);
					lblAuth.setEnabled(true);
					if (chckPAuth.isSelected()) {
						lblUser.setEnabled(true);
						lblPass.setEnabled(true);
						userField.setEnabled(true);
						passwordField.setEnabled(true);
					}
				} else {
					lblPHost.setEnabled(false);
					lblPPort.setEnabled(false);
					hostField.setEnabled(false);
					portField.setEnabled(false);
					chckPAuth.setEnabled(false);
					lblAuth.setEnabled(false);
					lblUser.setEnabled(false);
					lblPass.setEnabled(false);
					userField.setEnabled(false);
					passwordField.setEnabled(false);
				}
			}
		});
		
		chckPAuth.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (chckMainP.isSelected()) {
					lblPHost.setEnabled(true);
					lblPPort.setEnabled(true);
					hostField.setEnabled(true);
					portField.setEnabled(true);
					chckPAuth.setEnabled(true);
					lblAuth.setEnabled(true);
					if (chckPAuth.isSelected()) {
						lblUser.setEnabled(true);
						lblPass.setEnabled(true);
						userField.setEnabled(true);
						passwordField.setEnabled(true);
					} else {
						lblUser.setEnabled(false);
						lblPass.setEnabled(false);
						userField.setEnabled(false);
						passwordField.setEnabled(false);
					}
				} else {
					lblPHost.setEnabled(false);
					lblPPort.setEnabled(false);
					hostField.setEnabled(false);
					portField.setEnabled(false);
					chckPAuth.setEnabled(false);
					lblAuth.setEnabled(false);
					lblUser.setEnabled(false);
					lblPass.setEnabled(false);
					userField.setEnabled(false);
					passwordField.setEnabled(false);
				}
			}
		});
		
		chckUpdates.setSelected(storage.getAutoupdate());
		chckDebug.setSelected(storage.getAutodebug());
		chckPlugins.setSelected(storage.getAutoplugin());
		chckSupport.setSelected(storage.getSupportEnabled());
		chckSLogger.setSelected(storage.getSpecialLoggerEnabled());
		chckMainP.setSelected(storage.getMainProxyEnable());
		chckPAuth.setSelected(storage.getProxyAuth());
		
		if (chckMainP.isSelected()) {
			hostField.setText(storage.getPHost());
			portField.setText(storage.getPPort());
			userField.setText(storage.getPUser());
			passwordField.setText(new org.spigot.reticle.EncryptUtils("||_|;Reticle&^R]Fri3nds|V.<>^^*(").decrypt(storage.getPPass()));
		}
		
		if (chckMainP.isSelected()) {
			lblPHost.setEnabled(true);
			lblPPort.setEnabled(true);
			hostField.setEnabled(true);
			portField.setEnabled(true);
			chckPAuth.setEnabled(true);
			lblAuth.setEnabled(true);
			if (chckPAuth.isSelected()) {
				lblUser.setEnabled(true);
				lblPass.setEnabled(true);
				userField.setEnabled(true);
				passwordField.setEnabled(true);
			} else {
				lblUser.setEnabled(false);
				lblPass.setEnabled(false);
				userField.setEnabled(false);
				passwordField.setEnabled(false);
			}
		} else {
			lblPHost.setEnabled(false);
			lblPPort.setEnabled(false);
			hostField.setEnabled(false);
			portField.setEnabled(false);
			chckPAuth.setEnabled(false);
			lblAuth.setEnabled(false);
			lblUser.setEnabled(false);
			lblPass.setEnabled(false);
			userField.setEnabled(false);
			passwordField.setEnabled(false);
		}
		
		setResizable(false);
		setAlwaysOnTop(true);
		requestFocus();
		requestFocusInWindow();
	}
}
